/**
 * ============================================================================
 * @file generalstack.h
 * @author Tiago Sousa Rocha <tsrrocha@gmail.com>
 * @date 3 de Maio de 2019
 * @brief Este arquivo contem a definição de uma estrutura de dados chamada de Pilha.
 *
 * Esta biblioteca tem o objetivo de gerenciar uma pilha genérica de forma a processar
 * e armazenar qualquer tipo de objeto, gerenciando seu armazenamento (Add) e sua
 * remoção da pilha (remove).
 *
 * @see https://gitlab.com/tsrrocha/generalstack/tree/master
 * @note Código fundamental para a Pilha são os dois arquivos: generalstack.h e generalstack.c
 ============================================================================
 */
#ifndef STACK_GENERALSTACK_H_
#define STACK_GENERALSTACK_H_

#ifndef NULL
#define NULL 	0						/*!< Define NULL */
#endif

/**
 * @brief É uma definição para habilitar o DEBUG no console da aplicação.
 *
 */
#define DEBUG_STACK

/**
 *  @brief Define o tipo de dados BYTE como sendo unsigned char.
 */
typedef unsigned char BYTE;				/**< Define o tipo de dados BYTE */

/**
 *  @brief Define o tipo de dados IGeneralStack que representa um objeto (nó) da pilha.
 *
 *  Este nó contém um ponteiro que apontará para um objeto genérico a ser armazenado na pilha
 *  e também contém o ponteiro que aponta para o nó anterior ao objeto. Como trata-se de uma pilha
 *  o Último a entrar na pilha é o primeiro a sair (LIFO - Last In, First Out)
 *
 */
typedef struct nodeGeneralStack {
	void *obj;							/**< Objeto genérico armazenado em cada nó da Pilha. */
	struct nodeGeneralStack *prev;		/**< Apontador para o nó anterior da Pilha. */
} IGeneralStack ;						/**< Definição do nó da Pilha, chamado IGeneralStack. */

/**
 *  @brief Define a estrutura de dados TGeneralStack que é a pilha proriamente dita.
 *
 *  Esta estrutura é responsável por armazenar os objetos na pilha. Ela contém o ponteiro para
 *  o objeto que está no topo da Pilha e também a quantidade de itens contidos na Pilha.
 *
 */
typedef struct {
	IGeneralStack *top;					/**< Apontador para o nó que está no topo da Pilha. */
	unsigned int qty;					/**< Variável que armazena a quantidade de objetos na Pilha. */
} TGeneralStack;						/**< Definição da Pilha propriamente dita */

/**
 * @brief Esta função adiciona um novo objeto genérico no topo da Pilha.
 *
 * Adiciona um novo objeto, definido pelo ponteiro: @p obj, no topo da Pilha que
 * é definida pelo ponteiro: @p stk, no topo da Pilha.
 *
 * @param stk Ponteiro da Pilha e é do tipo: TGeneralStack *.
 * @param obj Ponteiro para o objeto genérico a ser adicionado à Pilha.
 * @return Retorna o sucesso da execução da função (= 1) ou a falha da execução (= 0).
 *
 * @see https://gitlab.com/tsrrocha/generalstack/tree/master
 * @note Código fundamental para a Pilha são os dois arquivos: generalstack.h e generalstack.c
 *
 */
BYTE addItemInTheGeneralStack(TGeneralStack *stk, void *obj);

/**
 * @brief Esta função remove o objeto que está no topo da Pilha.
 *
 * Retira da Pilha o objeto que está no topo,  retornando o endereço do objeto removido.
 *
 * @param stk Ponteiro da Pilha e é do tipo: TGeneralStack *.
 * @return Retorna um ponteiro para o Nó da Pilha que foi retirado da mesma. O Nó é do tipo @p IGeneralStack*
 *
 * @see https://gitlab.com/tsrrocha/generalstack/tree/master
 * @note Após retirar o objeto da Pilha é necessário liberar através da função free() a memória alocada para o objeto.
 *
 */
IGeneralStack*
removeItemOfGeneralStack(TGeneralStack *stk);

/**
 * @brief Esta função imprime os dados dos objetos contidos na Pilha.
 *
 * Esta função é usada para imprimir os objetos contidos na Pilha. A impressão é feita através do
 * ponteiro para a função responsável que irá imprimir o tipo de objeto que está armazenado na Pilha.
 *
 * @param *stk Ponteiro da Pilha e é do tipo: TGeneralStack *.
 * @param *fnprint Ponteiro para a função que irá imprimir os dados do objeto armazenado na Pilha.
 * @return Retorna o sucesso da execução da função (= 1) ou a falha da execução (= 0).
 *
 * @see https://gitlab.com/tsrrocha/generalstack/tree/master
 *
 */
BYTE
printItemsInTheStack (TGeneralStack *stk, BYTE (*fnprint) (void *obj2) );


#endif /* STACK_GENERALSTACK_H_ */
