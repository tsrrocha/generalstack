/*
 * generalstack.c
 *
 *  Created on: 3 de mai de 2019
 *      Author: Tiago Sousa Rocha <tsrrocha@gmail.com>
 */

#include "generalstack.h"
#include <malloc.h>

/**
 * @brief Esta função adiciona um novo objeto genérico no topo da Pilha.
 *
 * Adiciona um novo objeto, definido pelo ponteiro: @p obj, no topo da Pilha que
 * é definida pelo ponteiro: @p stk, no topo da Pilha.
 *
 * @param stk Ponteiro da Pilha e é do tipo: TGeneralStack *.
 * @param obj Ponteiro para o objeto genérico a ser adicionado à Pilha.
 * @return Retorna o sucesso da execução da função (= 1) ou a falha da execução (= 0).
 *
 * @see https://gitlab.com/tsrrocha/generalstack/tree/master
 * @note Código fundamental para a Pilha são os dois arquivos: generalstack.h e generalstack.c
 *
 */
BYTE
addItemInTheGeneralStack(TGeneralStack *stk, void *obj)
{
	IGeneralStack *new = (IGeneralStack *) malloc(sizeof(IGeneralStack));

	if (new == NULL) return 0;

	if (stk) {
		if (!stk->top) {
			new->obj = obj;
			new->prev = NULL;
			stk->top = new;
			stk->qty++;
			return 1;
		} else {
			new->obj = obj;
			new->prev = stk->top;
			stk->top = new;
			stk->qty++;
			return 1;
		}
	} else {
		// Libera memória alocada caso o ponteiro para a pilha (stk) for nulo.
		free(new);
	}
	return 0;
}

/**
 * @brief Esta função remove o objeto que está no topo da Pilha.
 *
 * Retira da Pilha o objeto que está no topo,  retornando o endereço do objeto removido.
 *
 * @param stk Ponteiro da Pilha e é do tipo: TGeneralStack *.
 * @return Retorna um ponteiro para o Nó da Pilha que foi retirado da mesma. O Nó é do tipo @p IGeneralStack*
 *
 * @see https://gitlab.com/tsrrocha/generalstack/tree/master
 * @note Após retirar o objeto da Pilha é necessário liberar através da função free() a memória alocada para o objeto.
 *
 */
IGeneralStack*
removeItemOfGeneralStack(TGeneralStack *stk) {
	IGeneralStack *aux;

	if ((stk) && (stk->top)) {
		aux = stk->top;
		// top->prev não aponta para nenhum objeto
		if (!stk->top->prev) {
			stk->top = NULL;
			stk->qty--;
		} else {
			stk->top = aux->prev;
			aux->prev = NULL;
			stk->qty--;
		}
	}
	return aux;
}

/**
 * @brief Esta função imprime os dados dos objetos contidos na Pilha.
 *
 * Esta função é usada para imprimir os objetos contidos na Pilha. A impressão é feita através do
 * ponteiro para a função responsável que irá imprimir o tipo de objeto que está armazenado na Pilha.
 *
 * @param *stk Ponteiro da Pilha e é do tipo: TGeneralStack *.
 * @param *fnprint Ponteiro para a função que irá imprimir os dados do objeto armazenado na Pilha.
 * @return Retorna o sucesso da execução da função (= 1) ou a falha da execução (= 0).
 *
 * @see https://gitlab.com/tsrrocha/generalstack/tree/master
 *
 */
BYTE
printItemsInTheStack (TGeneralStack *stk, BYTE (*fnprint) (void *obj2) )
{
	unsigned int idx = 0;
	IGeneralStack *atual;
	if ((stk->top) && (fnprint)) {
		atual = stk->top;
		for (idx=stk->qty; (idx > 0) ; idx--) {
			fnprint(atual->obj);
			atual = atual->prev;
		}
		return 1;
	}
	return 0;
}

