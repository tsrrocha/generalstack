## General Stack

Este projeto define uma estrutura de dados genérica do tipo Pilha, de forma a armazenar qualquer tipo de objeto. Por ser uma Pilha o novo objeto adicionado será armazenado no topo da Pilha. 

---

#### Exemplo de Uso:

###### Exemplo básico de uso da Pilha Genérica:

Observação: O tipo TCar utilizado não consta neste projeto, mas é apenas para demonstrar que o tipo TGeneralStack poderia armazenar qualquer tipo de objeto, como por exemplo: Laranja, Banana, Carro e etc.

```c
#include <stdio.h>
#include <stdlib.h>
#include "usertypes.h"
#include "generalstack.h"

int main(void) {
	TGeneralStack *pilha;
	TCar *grandsiena;
	TCar *palio;
	TCar *celta;
	IGeneralStack *aux;

	grandsiena = (TCar *) malloc(sizeof(TCar));
	if (grandsiena) {
		sprintf(grandsiena->modelo, "%s", "Grand Siena");
		grandsiena->anofabricacao = 2015;
		grandsiena->anomodelo = 2016;
	}

	palio = (TCar *) malloc(sizeof(TCar));
	if (palio) {
		sprintf(palio->modelo, "%s", "Palio");
		palio->anofabricacao = 2013;
		palio->anomodelo = 2013;
	}

	celta = (TCar *) malloc(sizeof(TCar));
	if (celta) {
		sprintf(celta->modelo, "%s", "Celta");
		celta->anofabricacao = 2001;
		celta->anomodelo = 2002;
	}

	pilha = (TGeneralStack *) malloc(sizeof(TGeneralStack));
	if (pilha) {
		addItemInTheGeneralStack (pilha, grandsiena);
		printf("==DEBUG_APP==\tAdiciona grand siena, atualmente tem %d objetos na pilha\n", pilha->qty);
		addItemInTheGeneralStack (pilha, palio);
		printf("==DEBUG_APP==\tAdiciona palio, atualmente tem %d objetos na pilha: %d\n", pilha->qty);
		addItemInTheGeneralStack (pilha, celta);
		printf("==DEBUG_APP==\tAdiciona celta, atualmente tem %d objetos na pilha: %d\n", pilha->qty);

		printf("\n==DEBUG_APP==\tImprime os objetos da Pilha, utilizando a função printDataCar para exibir os dados de cada TCar.\n");
		printItemsInTheStack(pilha, printDataCar);

		aux = (IGeneralStack *) removeItemOfGeneralStack(pilha);
		printf("==DEBUG_APP==\tRetira um objeto TCar do topo da Pilha. Modelo: %s\n", ((TCar *)aux->obj)->modelo);
		if (aux) free(aux);

		aux = (IGeneralStack *) removeItemOfGeneralStack(pilha);
		printf("==DEBUG_APP==\tRetira um objeto TCar do topo da Pilha. Modelo: %s\n", ((TCar *)aux->obj)->modelo);
		if (aux) free(aux);

		aux = (IGeneralStack *) removeItemOfGeneralStack(pilha);
		printf("==DEBUG_APP==\tRetira um objeto TCar do topo da Pilha. Modelo: %s\n", ((TCar *)aux->obj)->modelo);
		if (aux) free(aux);

		printf("\n==DEBUG_APP==\tImprime os itens da Pilha\n");
		printItemsInTheStack(pilha, printDataCar);

		printf("==DEBUG_APP==\tFIM\n\n", pilha->qty);
	}

	return EXIT_SUCCESS;
}
```





